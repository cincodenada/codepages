// set the dimensions and margins of the graph
var margin = {top: 0, right: 0, bottom: 0, left: 0},
  width = 5000 - margin.left - margin.right,
  height = 5000 - margin.top - margin.bottom;

var container = d3.select("#char_chart")

let boxes = [];
let is_focused = false;
let filter = "";

if(document.location.search) {
  query = decodeURIComponent(document.location.search.substring(1));
  document.querySelector("#filter").value = query;
  filter = query.split("");
}

// Read data
d3.csv('totals.csv', function(data) {
  data = data.map(row => ({
    char: row.char,
    alias: row.alias,
    description: row.description,
    charcode: parseInt(row.charcode),
    count: parseInt(row.count),
    total: parseInt(row.total),
  }));

  // char, charcode, count, total
  data.sort((a, b) => {
    if(a.charcode == b.charcode) {
      return b.count - a.count;
    } else {
      return a.charcode - b.charcode;
    }
  })

  let cur_bits = [];
  let others = [];
  let split_thresh = 0.2;
  let flip_thresh = 0.75; // w/h thresh above which we split vertically

  let cur_total = 0;
  let cur_depth = 0;
  let cur_w = 1;
  let cur_h = 1;
  for(let idx in data) {
    let d = data[idx];

    cur_total += d.count;
    let remainder = d.total - cur_total;

    if(d.count > 1) {
      let next_w = cur_w;
      let next_h = cur_h;
      let box_w = cur_w;
      let box_h = cur_h;
      let proportion = remainder/(remainder+d.count);
      if(proportion < split_thresh) {
        // Calculate size of corner box
        let side_factor = Math.sqrt(proportion)
        next_w = cur_w*side_factor;
        next_h = cur_h*side_factor;
      } else {
        if(cur_w/cur_h > flip_thresh) {
          // Split vertically
          next_w = cur_w*proportion;
          box_w = cur_w-next_w;
        } else {
          next_h = cur_h*proportion;
          box_h = cur_h-next_h;
        }
      }

      let cur_box = d;
      cur_box.w = box_w;
      cur_box.h = box_h;
      cur_box.x = 1-cur_w;
      cur_box.y = 1-cur_h;
      cur_box.depth = cur_depth;

      cur_bits.push(cur_box);

      cur_w = next_w;
      cur_h = next_h;
      cur_depth++;
    } else {
      others.push(d);
    }

    if(idx == (data.length-1) || data[+idx+1].charcode != d.charcode) {
      if(others.length) {
        let last;
        if(others.length == 1) {
          last = others[0];
        } else {
          last = {
            char: '',
            alias: "(+"+others.length+")",
            description: "Other (" + others.map(d => d.char).join(',') + ")",
            charcode: d.charcode,
            count: others.length,
            total: d.total,
            others: others,
          }
        }
        last.w = cur_w;
        last.h = cur_h;
        last.x = 1-cur_w;
        last.y = 1-cur_h;
        last.depth = cur_depth;
        cur_bits.push(last);
      }
      boxes[d.charcode] = cur_bits;

      cur_bits = [];
      others = [];
      cur_total = 0;
      cur_depth = 0;
      cur_w = 1;
      cur_h = 1;
    }
  }

  update(boxes);
})

let codepage_names = [];
d3.csv('codepage_idx.csv', data => {
  for(d of data) {
    codepage_names[d.id] = d.codepage;
  }
});
let codepage_map = {};
d3.csv('codepage_map.csv', data => {
  for(d of data) {
    codepage_map[d.char + "@" + d.charcode] = d.pages.split(',');
  }
});
let popular = {};
d3.csv('popularity.csv', data => {
  for(d of data) {
    popular[d.id] = {
      rating: d.rating,
      display: d.DisplayName,
    }
  }
});

let infobox = d3.select("#charinfo_tab");

function showtab(which) {
	d3.selectAll('#info div').property('hidden', true)
	d3.select('#' + which + '_tab').property('hidden', false)
}
d3.selectAll('ul.tabs a').on('click', () => {
	which = d3.event.target.href.split('#')[1];
	showtab(which);
	d3.event.stopPropagation();
	d3.event.preventDefault();
});

function focus(datum) {
  if(is_focused) {
    update(boxes)
  } else {
    update([boxes[datum[0].charcode]]);
  }
  is_focused = !is_focused;
}

function hover(d) {
  let codekey = d.char + "@" + d.charcode;
  let hexcode = '0x' + d.charcode.toString(16).padStart(2, '0')
  if(codekey in codepage_map) {
    let info = `&#x276c;${d.char}&#x276d;: ${d.description}<br/>`
    let tops = []
    let others = []
    for(id of codepage_map[codekey]) {
      if(id in popular) {
        tops.push(popular[id])
      } else {
        others.push(codepage_names[id])
      }
    }
    tops.sort((a, b) => b.rating - a.rating);
    info += `Listed at position ${d.charcode} (${hexcode}) in ${tops.length} top codepage${tops.length == 1 ? '' : 's'}${tops.length == 0 ? '' : ':'}<br/>`;
    if(tops.length) {
      info += tops.map(p => p.display).join("<br/>")
    }
    info += `<hr/>And ${others.length} other codepage${others.length == 1 ? '' : 's'}:<br/><small>${others.join(", ")}</small>`
    infobox.html(info);
  } else if(d.others) {
    let info = `Characters found at position ${d.charcode} (${hexcode}) once:<br/>`
    info += d.others.map(d => {
      let codekey = d.char + "@" + d.charcode;
      return `&#x276c;${d.alias ? d.alias : d.char}&#x276d;: ${codepage_names[codepage_map[codekey]]}`;
    }).join('<br/>')
    infobox.html(info);
  } else {
    infobox.text(d.description);
  }
	showtab('charinfo')
}

function check_match(datum, filter, match_all) {
  if(filter == "") { return false; }
  let to_find = filter;
  for(c of datum) {
    to_find = to_find.filter(found => found != c.char);
  }
  // This is not efficient, but eh for now
  if(match_all) {
    return (to_find.length == 0)
  } else {
    return (to_find.length < filter.length)
  }
}

function applyFilter() {
  let match_all = document.querySelector('#match_all').checked;
  update(boxes.filter(d => check_match(d, filter, match_all)));
}

function update(data) {
  console.log(data);

  let boxw = Math.max(50, 800/data.length);
  let boxh = boxw*5/4;

  let pos_x = (d) => boxw + boxw*d.x;
  let pos_y = (d) => boxh + boxh*d.y;

  let selection = container.selectAll("div.box").data(data)
  let match_all = document.querySelector('#match_all').checked;

  let merged = selection
    .enter()
    .append("div")
      .on("click", focus)
      .attr('class','box')
    .merge(selection)
      .style('width', boxw + 'px')
      .style('height', boxh + 'px')
      .style('font-size', boxh/2 + 'px')
    .classed('hilit', d => check_match(d, filter, match_all))

  selection.exit().remove()

  let subselection = merged.selectAll("div.subbox").data((d) => d)

  let enter = subselection.enter()
    .append("div")
      .on("mouseover", hover)
      .attr('class','subbox')

  enter.append("div").attr('class','label')
  enter.merge(subselection)
      .attr('title', (d) => d.description)
      .style('left', (d) => d.x*100 + "%")
      .style('top', (d) => d.y*100 + "%")
      .style('width', (d) => d.w*100 + '%')
      .style('height', (d) => d.h*100 + '%')
      .style('font-size', (d) => (Math.min(d.h, d.w)*(d.alias ? 50 : 100) + '%'))
      .style("background-color", (d) => "rgb(0,"+Math.max(0,128-d.depth*5)+",0)")
      .select('.label')
        .html((d) => (d.alias
          ? d.alias.substring(1,d.alias.length-1).split("").join("<br/>")
          : d.char)
        )

  subselection.exit().remove()

  let matches = d3.selectAll('.hilit').size();
  d3.select("#result").text(`Found ${matches} match${matches == 1 ? '' : 'es'}`)
}

d3.select("#filter").on("input", function() {
  console.log("Filtering: " + this.value);
  filter = this.value.split("");
  update(boxes);
})
d3.selectAll("input[type=radio]").on("input", function() {
  update(boxes)
})
