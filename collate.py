#!/usr/bin/env python3
import os
import csv
import re

# Inspired by https://cybre.space/@cinebox/103461530608457063

mapdir = "charmaps"
splitter = re.compile('\\s+')
charfmt = re.compile('^\\<U[a-zA-Z0-9]{4,}\\>$')

out_chars = []

for codepage in os.listdir(mapdir):
  fullpath = os.path.join(mapdir, codepage)
  if os.path.isfile(fullpath):
    print("Processing {}...".format(fullpath))
    with open(fullpath, 'r') as f:
      in_map = False
      comment_char = "%"
      escape_char = "/"

      for line in f:
        if line == "":
          continue

        parts = splitter.split(line.strip(), 2)
        which = parts[0]
        if which == "<code_set_name>":
          name = parts[1]
        elif which == "<comment_char>":
          comment_char = "%"
        elif which == "<escape_char>":
          escape_char = "/"
        elif which == "CHARMAP":
          in_map = True
        elif which == "END" and parts[1] == "CHARMAP":
          in_map = False
        elif in_map and charfmt.match(which):
          if len(parts) == 3:
            (unicode_char, charcode, desc) = parts
            if len(charcode) == 4:
              out_chars.append([
                codepage,
                int(charcode[2:4], 16),
                "\\u{}".format(unicode_char[2:-1]).encode().decode('unicode-escape'),
                desc
              ])
          else:
            print("Unexpected line with {} elements: {}".format(len(parts),line))
        else:
          pass

with open('collated.csv', 'w') as csvfile:
  outcsv = csv.writer(csvfile, quoting=csv.QUOTE_NONNUMERIC)
  for c in out_chars:
    outcsv.writerow(c)
